<div class="slidePanel-content site-sidebar-content">
  <div class="panel-body">
    <div class="reset-filter">
      <a href="#">Reset Filter</a>
    </div>
      <div class="form-group">
          <label>Creator</label>
          <select data-plugin="selectpicker">
              <option>Creator name</option>
              <option>Category Name</option>
              <option>Category Name</option>
          </select>
      </div>
      <div class="form-group">
          <label>Type</label><select data-plugin="selectpicker">
              <option>Image</option>
              <option>Category Name</option>
              <option>Category Name</option>
          </select>
      </div>
      <div class="example">
      <label>Date Added</label>
        <div class="input-group">
          <span class="input-group-addon">
            <i class="icon md-calendar" aria-hidden="true"></i>
          </span>
          <input type="text" class="form-control" data-plugin="datepicker">
        </div>
      </div>
      <div class="example">
      <label>Last Modified</label>
        <div class="input-group">
          <span class="input-group-addon">
            <i class="icon md-calendar" aria-hidden="true"></i>
          </span>
          <input type="text" class="form-control" data-plugin="datepicker" data-multidate="true">
        </div>
      </div>
  </div>
  <div class="panel-footer">
      <div class="footer-btns">
          <button type="submit" class="btn btn-primary btn-block grey-btn btn-lg margin-top-40 waves-effect waves-light">Cancel</button>
          <button type="submit" class="btn btn-primary btn-block btn-lg margin-top-40 waves-effect waves-light">Search</button>
      </div>
  </div>

  </div>
  <div class="slidePanel-handler"></div>
  <div class="slidePanel-loading slidePanel-loading-show">
  <div class="loader loader-default"></div>
  </div>
</div>
